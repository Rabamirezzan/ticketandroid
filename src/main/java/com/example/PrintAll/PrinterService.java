package com.example.PrintAll;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothSocket;
import android.graphics.Bitmap;
import android.os.Build;
import android.util.Log;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.graphics.ZebraImageI;
import com.zebra.sdk.graphics.internal.*;
import com.zebra.sdk.util.internal.ZPLUtilities;

import java.io.*;
import java.nio.charset.Charset;
import java.util.*;

/**
 * Created by rramirezb on 25/03/2015.
 */
public class PrinterService {

    private static PrinterService instance;

    public static PrinterService getInstance() {
        if (instance == null) {
            instance = new PrinterService();
        }
        return instance;
    }

    private PrinterService() {

    }

    public BluetoothAdapter getAdapter() {
        BluetoothAdapter adapter = BluetoothAdapter.getDefaultAdapter();
        return adapter;
    }

    public List<BluetoothDevice> getDevices() {
        return getDevices(getAdapter());
    }

    public List<BluetoothDevice> getDevices(BluetoothAdapter adapter) {
        Set<BluetoothDevice> devices = adapter.getBondedDevices();
        return new ArrayList<BluetoothDevice>(devices);
    }

    public static List<String> getAsStrings(List<BluetoothDevice> devices) {
        List<String> list = new ArrayList<String>();
        for (int i = 0; i < devices.size(); i++) {
            BluetoothDevice bluetoothDevice = devices.get(i);
            String name = bluetoothDevice.getName();
            list.add(name);
        }
        return list;
    }

    public void print(BluetoothDevice device, String fileImage) {
        BluetoothSocket socket = open(device);
        sendImage(socket, fileImage);
    }

    private static final UUID UUID_SPP = UUID.fromString("00001101-0000-1000-8000-00805F9B34FB");
    private BluetoothSocket open(BluetoothDevice device) {

        BluetoothSocket socket = null;
        try {
            // Standard SerialPortService ID
            UUID uuid = UUID_SPP;//UUID.fromString("00001101-0000-1000-8000-00805f9b34fb");
            socket = device.createRfcommSocketToServiceRecord(uuid);
            socket.connect();


        } catch (NullPointerException e) {
            e.printStackTrace();

        } catch (Exception e) {
            e.printStackTrace();
        }


        return socket;
    }

    public OutputStream openOutputStream(BluetoothSocket socket) {
        Charset charset = Charset.forName("UTF-8");
        OutputStream outPutStreamWriter = null;
        try {
            outPutStreamWriter = socket.getOutputStream()
            ;
        } catch (IOException e) {
            e.printStackTrace();
        }

        return outPutStreamWriter;

    }

    //string zplToSend = "^XA" + "^MNN" + "^LL500" + "~DYE:LOGO,P,P," + binaryData.Length + ",," + zplImageData+"^XZ";
    //string printImage_CPCL = "^XA^FO115,50^IME:LOGO.PNG^FS^XZ";
    private static final String ZPL_TO_SEND = "^XA" + "^MNN" + "^LL500" + "~DYE:LOGO,P,P,%s,,%s^XZ";
    private static final String PRINT_IMAGE = "^XA^FO115,50^IME:LOGO.PNG^FS^XZ";

    public void sendImage(BluetoothSocket socket, String file) {
//        OutputStreamWriter output = openOutputStream(socket);
//        byte[] data = getRawImage(file);
//        String stringData = getStringImage(data);
//        try {
//            output.write(String.format(ZPL_TO_SEND, data.length, stringData));
//            output.flush();
//            output.write(PRINT_IMAGE);
//            output.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//
//        try {
//            socket.close();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
    }

    public static byte[] getRawImage(String file) {

        ByteArrayOutputStream out = new ByteArrayOutputStream();

        final int SIZE = 1024;
        byte[] buff = new byte[SIZE];
        byte[] data = new byte[]{};
        try {
            FileInputStream input = new FileInputStream(new File(file));
            int l = -1;
            while ((l = input.read(buff)) != -1) {
                out.write(buff, 0, l);
            }
            out.flush();

            data = out.toByteArray();
            out.close();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return data;
    }


    public void printImage_CPCL(BluetoothDevice device, Bitmap bitmap) {
        BluetoothSocket socket = open(device);
        OutputStream output = openOutputStream(socket);


        try {
            ByteArrayOutputStream out = printImage_CPCL(new ZebraImageAndroid(bitmap));
            byte[] data = out.toByteArray();
            Log.i("PRINTER", "Data length " + data.length);
            output.write(data, 0, data.length);
            output.flush();

        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Thread.sleep(3000);
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    protected ZebraImageInternal scaleImage(int var1, int var2, ZebraImageInternal var3) {
        var3.scaleImage(var1, var2);
        return var3;
    }

    public ByteArrayOutputStream printImage_CPCL(ZebraImageI image) throws IOException {
        return printImage_CPCL(image, 0, 0, false);

    }

    public ByteArrayOutputStream printImage_CPCL(ZebraImageI var1, int x, int y, boolean var6) throws IOException {

        //ZebraImageInternal var7 = scaleImage(var4, var5, (ZebraImageInternal)var1);
        ZebraImageInternal var7 = ((ZebraImageInternal) var1);
        int var8 = (var1.getWidth() + 7) / 8;

        ByteArrayOutputStream printerConnection = new ByteArrayOutputStream();


        ByteArrayOutputStream var9 = new ByteArrayOutputStream();

        var9.write("! U1 JOURNAL\r\n! U1 SETFF 50 2\r\n".getBytes());
        String var10 = var6 ? "" : "! 0 200 200 " + var7.getHeight() + " 1\r\n";
        String var11 = var6 ? "" : "FORM\r\nPRINT\r\n";
        var9.write(var10.getBytes());
        var9.write("CG ".getBytes());
        var9.write(String.valueOf(var8).getBytes());
        var9.write(" ".getBytes());
        var9.write(String.valueOf(var7.getHeight()).getBytes());
        var9.write(" ".getBytes());
        var9.write(String.valueOf(x).getBytes());
        var9.write(" ".getBytes());
        var9.write(String.valueOf(y).getBytes());
        var9.write(" ".getBytes());
        printerConnection.write(var9.toByteArray());
        PrinterConnectionOutputStreamFixed var12 = new PrinterConnectionOutputStreamFixed(printerConnection);
        CompressedBitmapOutputStreamCpcl var13 = new CompressedBitmapOutputStreamCpcl(var12);
        DitheredImageProvider.getDitheredImage(var7, var13);
        var13.close();
        var12.close();
        printerConnection.write("\r\n".getBytes());
        printerConnection.write(var11.getBytes());
        printerConnection.flush();
        printerConnection.close();
        //Log.i("PRINTER", "The data: "+printerConnection.toString());

        return printerConnection;
    }

    public ByteArrayOutputStream printImage_ZPL(
            ZebraImageI var1,
            int var2,
            int var3,
//            int var4,
//            int var5,
            boolean var6) throws ConnectionException {
        ZebraImageInternal var7 = (ZebraImageInternal) var1;//this.scaleImage(var4, var5, (ZebraImageInternal)var1);

        ByteArrayOutputStream printerConnection = new ByteArrayOutputStream();
        try {
            int var8 = (var7.getWidth() + 7) / 8;
            int var9 = var8 * var7.getHeight();
            String var10 = this.getBodyHeader(var2, var3, var6, var8, var9);
            String var11 = ZPLUtilities.replaceAllWithInternalCharacters(var10);
            printerConnection.write(var11.getBytes());
            PrinterConnectionOutputStreamFixed var12 = new PrinterConnectionOutputStreamFixed(printerConnection);
            CompressedBitmapOutputStreamZpl var13 = new CompressedBitmapOutputStreamZpl(var12);


            DitheredImageProvider.getDitheredImage(var7, var13);
            var13.close();
            var12.close();


            if (!var6) {
                String var14 = ZPLUtilities.decorateWithFormatPrefix("^XZ");
                printerConnection.write(var14.getBytes());
            }
        } catch (IOException var15) {
            throw new ConnectionException(var15.getMessage());
        }

        return printerConnection;

    }

    private String getBodyHeader(int var1, int var2, boolean var3, int var4, int var5) {
        StringBuffer var6 = new StringBuffer();
        var6.append("^FO");
        var6.append(var1);
        var6.append(",");
        var6.append(var2);
        var6.append("^GFA");
        var6.append(",");
        var6.append(var5);
        var6.append(",");
        var6.append(var5);
        var6.append(",");
        var6.append(var4);
        var6.append(",");
        String var7 = var6.toString();
        if (!var3) {
            var7 = "^XA" + var7;
        }

        return var7;
    }


    private int getPrintRow(int[] data) {
        int z = 0;
        for (int i = data.length - 1; i >= 0; i--) {
            if (data[i] != -1) {
                z |= 1 << i;
            }

        }
        return z;
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public void printOnS30(BluetoothDevice device, Bitmap bitmap) {


        int height = bitmap.getHeight();
        int width = bitmap.getWidth();
        System.out.printf("Image bitmap %s %s\n", width, bitmap.getHeight());
        try {
            System.out.println("Open device");
            BluetoothSocket s = this.open(device);
            System.out.println("Open OutputStream");
            OutputStream output =
                    //new ByteArrayOutputStream();
                    this.openOutputStream(s);
//            ZebraImageAndroid image = new ZebraImageAndroid(bitmap);
//
//            ByteArrayOutputStream bitmapRaw = new ByteArrayOutputStream();
//
////        Bitmap.


            ByteArrayOutputStream ditheredImage = new ByteArrayOutputStream();
            getDitheredImage(new ZebraImageAndroid(bitmap), ditheredImage);

            System.out.printf("Scale gray image %s\n", ditheredImage.size());

            byte[] ditheredImageBytes = ditheredImage.toByteArray();

            for (int i = 0; i<ditheredImageBytes.length; i++){
                ditheredImageBytes[i] = (byte)((~ditheredImageBytes[i])&0xFF);
            }
            List<byte[]> fragments = new ArrayList<byte[]>();

            final int MAX_HEIGHT = 255;
            fragments = cropBytesByHeight(width, height, MAX_HEIGHT, ditheredImageBytes);


//            byte[] ditheredImageBytesCrop = new byte[((int)(width+7)/8)*height];
//            ditheredImageBytesCrop = Arrays.copyOfRange(ditheredImageBytes, 0, ditheredImageBytesCrop.length);

            BufferedOutputStream writer = new BufferedOutputStream((output));
            int rowSize = (int)((width+7)/8);
            byte[] FF = new byte[]{0x0C};
            byte[] CAN = new byte[]{0x18};
            int dyH = (int)(height/256);
            int dyL = (int) (height%256);
            byte[] ESC_W = new byte[]{0x1B, 0x57, 0, 0, 0, 0, (byte)128, 1, (byte)dyL, (byte)dyH};
            for (int i = 0; i < fragments.size(); i++) {
                byte[] bytes =  fragments.get(i);
                int _height = (int)(bytes.length/rowSize);
                System.out.printf("part %s length: %s, height %s\n", i,bytes.length, _height);
                ByteArrayOutputStream data = print(width, _height,
                        bytes);
                byte[] dataBytes = data.toByteArray();

                writer.write(CAN);
                writer.write(dataBytes, 0, dataBytes.length);
                writer.write(FF);

            }

            writer.flush();
            //writer.flush();


            try {
                Thread.sleep(7000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
            writer.close();
            s.close();
        } catch (Exception e) {
            e.printStackTrace();
        }


        System.out.printf("Ends of s30 data (%s, %s) %s\n", width, height, width*height);
    }

    @TargetApi(Build.VERSION_CODES.GINGERBREAD)
    public static List<byte[]> cropBytesByHeight(int width, int height, final int maxHeight, byte[] data){

        int lengthAcc = 0;
        final int MAX_HEIGHT = maxHeight;
        final int NEW_WIDTH = (int)((width+7)/8);
        final int MAX_LENGHT = MAX_HEIGHT*NEW_WIDTH;
        final int IMAGE_LENGTH = NEW_WIDTH*height;
        List<byte[]> result = new ArrayList<byte[]>();
        while(lengthAcc<IMAGE_LENGTH){
            int _length = MAX_LENGHT;
            _length = (lengthAcc+MAX_LENGHT<=IMAGE_LENGTH)?(MAX_LENGHT):(IMAGE_LENGTH-lengthAcc);
            int start = lengthAcc;
            int end =  lengthAcc+_length;
            //System.out.printf("%s %s\n", start, end);
            byte[] copy = Arrays.copyOfRange(data, start, end);
            result.add(copy);
            lengthAcc += _length;
        }

        return result;
    }

    private ByteArrayOutputStream print(int width, int height, byte[] data) {
        ByteArrayOutputStream stream;
        stream = new ByteArrayOutputStream();
        byte[] printImageCommand = new byte[]{0x1B, 0x58, 0x34};
        int _x = (width + 7) / 8;
        byte[] x = new byte[]{(byte) ((_x))};
        byte[] y = new byte[]{(byte) height};
        try {
            stream.write(printImageCommand);
            stream.write(x);
            stream.write(y);
            stream.write(data, 0, data.length);

        } catch (IOException e) {
            e.printStackTrace();
        }
        return stream;
    }

    public static void getDitheredImage(ZebraImageInternal var0, OutputStream var1) throws IOException {
        int var2 = var0.getWidth();
        int var3 = var0.getHeight();
        getDitheredImage(var2, var3, var0, var1);
    }

    protected static void getDitheredImage(int width, int height, ZebraImageInternal image, OutputStream ditheredImage) throws IOException {
        int[] row0 = image.getRow(0);
        int[] row1 = image.getRow(1);
        int newWidth = width / 8 + (width % 8 == 0?0:1);
        int var7 = 8 - width % 8;
        if(var7 == 8) {
            var7 = 0;
        }

        byte[] row = new byte[newWidth];
        byte var9 = 0;

        int i;
        for(i = 0; i < width; ++i) {
            row0[i] = convertByteToGrayscale(row0[i]);
        }

        for(i = 0; i < height; ++i) {
            int j;
            for(j = 0; j < row.length; ++j) {
                row[j] = 0;
            }

            j = 0;

            for(int k = 0; k < width; ++k) {
                if(k % 8 == 0) {
                    var9 = -128;
                }

                int var13 = row0[k];
                j = k / 8;
                int var14 = var13 >= 128?-1:0;
                row[j] = (byte)(row[j] | var9 & var14);
                int var15 = var13 - (var14 & 255);
                if(k < width - 1) {
                    row0[k + 1] += 7 * var15 / 16;
                }

                if(k > 0 && i < height - 1) {
                    row1[k - 1] += 3 * var15 / 16;
                }

                if(i < height - 1) {
                    if(k == 0) {
                        row1[k] = convertByteToGrayscale(row1[k]);
                    }

                    row1[k] += 5 * var15 / 16;
                }

                if(i < height - 1 && k < width - 1) {
                    row1[k + 1] = convertByteToGrayscale(row1[k + 1]);
                    row1[k + 1] += 1 * var15 / 16;
                }

                var9 = (byte)((var9 & 255) >>> 1);
            }

            row[j] = (byte)(row[j] | 255 >>> 8 - var7);
            ditheredImage.write(row);
            row0 = row1;
            row1 = image.getRow(i + 2);
        }

    }

    private static int convertByteToGrayscale(int var0) {
        int var1 = (var0 & 16711680) >>> 16;
        int var2 = (var0 & '\uff00') >>> 8;
        int var3 = var0 & 255;
        int var4 = (var1 * 30 + var2 * 59 + var3 * 11) / 100;
        if(var4 > 255) {
            var4 = 255;
        } else if(var4 < 0) {
            var4 = 0;
        }

        return var4;
    }

}
