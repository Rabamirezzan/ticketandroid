package com.example.PrintAll;

import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Canvas;
import android.os.Build;
import android.util.AttributeSet;
import android.webkit.WebView;

/**
 * Created by rramirezb on 08/04/2015.
 */
public class WebViewPrinter extends WebView {



    private OnSizeChangedListener listener;

    public WebViewPrinter(Context context) {
        super(context);
    }

    public WebViewPrinter(Context context, AttributeSet attrs) {
        super(context, attrs);
    }

    public WebViewPrinter(Context context, AttributeSet attrs, int defStyleAttr) {
        super(context, attrs, defStyleAttr);
    }

    @TargetApi(Build.VERSION_CODES.LOLLIPOP)
    public WebViewPrinter(Context context, AttributeSet attrs, int defStyleAttr, int defStyleRes) {
        super(context, attrs, defStyleAttr, defStyleRes);
    }

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    public WebViewPrinter(Context context, AttributeSet attrs, int defStyleAttr, boolean privateBrowsing) {
        super(context, attrs, defStyleAttr, privateBrowsing);
    }

    public void setOnSizeChangedListener(OnSizeChangedListener listener){
        this.listener = listener;
    }
    @Override
    protected void onSizeChanged(int w, int h, int ow, int oh) {
        super.onSizeChanged(w, h, ow, oh);
        if(listener!=null){
            listener.onSizeChanged(w, h, ow, oh);


        }
    }


}
