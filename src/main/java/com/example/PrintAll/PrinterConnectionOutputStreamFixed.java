package com.example.PrintAll;

import com.zebra.sdk.comm.Connection;
import com.zebra.sdk.comm.ConnectionException;
import com.zebra.sdk.comm.internal.ConnectionBuilderInternal;

import java.io.IOException;
import java.io.OutputStream;

/**
 * Created by rramirezb on 26/03/2015.
 */
public class PrinterConnectionOutputStreamFixed extends OutputStream {
    private OutputStream printerConnection;



    public PrinterConnectionOutputStreamFixed(OutputStream var1) {
        this.printerConnection = var1;
    }

    public void write(byte[] var1) throws IOException {

            this.printerConnection.write(var1);

    }

    public void write(byte[] var1, int var2, int var3) throws IOException {

            this.printerConnection.write(var1, var2, var3);

    }

    public void write(int var1) throws IOException {
        throw new IOException("This method is not implemented.");
    }

    public void openPrinterConnection() throws ConnectionException {
        //this.printerConnection.open();
    }

    public void closePrinterConnection() throws ConnectionException {
        try {
            this.printerConnection.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
