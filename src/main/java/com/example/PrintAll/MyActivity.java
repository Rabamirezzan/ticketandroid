package com.example.PrintAll;

import android.annotation.TargetApi;
import android.app.Activity;
import android.bluetooth.BluetoothDevice;
import android.content.Context;
import android.graphics.*;
import android.os.*;
import android.util.Log;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;
import android.widget.*;
import com.opesystems.smartorder.ticket.CustomerTicket;
import com.opesystems.smartorder.ticket.EmployeeTicket;
import com.opesystems.smartorder.ticket.OrderTicket;
import com.opesystems.smartorder.ticket.ProductTicket;
import com.opesystems.ticket.android.TicketGenerator;
import com.opesystems.utilities.Products;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;


public class MyActivity extends Activity {
    /**
     * Called when the activity is first created.
     */

    ViewGroup parent;
    TextView textView;
    Spinner spinner;
    //String htmlFile = "file:///android_asset/ticket.html";
    String htmlFile = "http://10.2.149.211:19810/PrintAll/assets/ticket.html";
    //192.168.1.64
    //192.168.9.86
//    String htmlFile = "http://192.168.1.64:19810/PrintAll/assets/ticket.html";
    //String htmlFile = "http://169.254.123.229:19810/PrintAll/assets/ticket.html";
    Button printButton;
    Button loadButton;
    List<BluetoothDevice> devices;
    TextView widthField;
    TicketGenerator ticketGen;

    @TargetApi(Build.VERSION_CODES.HONEYCOMB)
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.main);



        widthField = (TextView) findViewById(R.id.width);
        parent = (ViewGroup) findViewById(R.id.parent);


        spinner = (Spinner) findViewById(R.id.spinner);
        printButton = (Button) findViewById(R.id.printButton);
        loadButton = (Button) findViewById(R.id.loadButton);
        View.OnClickListener listener =
//                new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                BluetoothDevice device = devices.get(spinner.getSelectedItemPosition());
//
//            }
//        };

                new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                hideKeyboard(MyActivity.this, v);
                v.setEnabled(false);
                if (!devices.isEmpty()) {
                    BluetoothDevice device = devices.get(spinner.getSelectedItemPosition());
                }
                //PrinterService.print(device, getFile().getAbsolutePath());

                boolean print = v.getId()==R.id.printButton;
                saveBitmap(print);
//                new PrinterService().sendImage(device, bitmap, MyActivity.this);
//                Toast.makeText(MyActivity.this, "Address " + device.getAddress(), Toast.LENGTH_LONG).show();

            }
        };
        printButton.setOnClickListener(listener);
        loadButton.setOnClickListener(listener);

        devices = PrinterService.getInstance().getDevices();
        spinner.setAdapter(
                new ArrayAdapter<String>(
                        getApplicationContext(),
                        android.R.layout.simple_spinner_item,
                        PrinterService.getAsStrings(devices)));


        //startService(new Intent(this, ImagePrint.class));
        OrderTicket ticket = new OrderTicket();
        List<ProductTicket> productsList = new ArrayList<ProductTicket>();
        productsList.addAll(Products.getProducts());
        ticket.selectedProducts = (productsList);
        ticket.selectedPromotions = Products.getPromotions();

        ticket.companyName= ("Bepensa");
        ticket.noteType = ("Nota de Pedido");
        ticket.productsHeader= ("Productos seleccionados");
        ticket.promotionsHeader = "Promociones seleccionadas";
        ticket.footer = "Lávate las manos antes de comer y después de ir al baño";
        ticket.date = new SimpleDateFormat("dd/MM/yy, hh:mm a").format(new Date());

        ticket.customer = new CustomerTicket();
        ticket.customer.code = "RABR8103177AA";
        ticket.customer.name = "La Tienda Todo Caro";
        ticket.customer.address = "Avenida Siempreviva 742, Springfield";

        ticket.employee = new EmployeeTicket();
        ticket.employee.name = "Raúl Ramírez Bazán";

        ticket.showProducts = !ticket.selectedProducts.isEmpty();
        ticket.showReplacements = !ticket.selectedReplacements.isEmpty();
        ticket.showPromotions = !ticket.selectedPromotions.isEmpty();
        ticketGen = new TicketGenerator(
                parent,R.id.ticketLayout, htmlFile, null,
                new File(Environment.getExternalStorageDirectory(), "tickets_test"));


        printDensity();
    }

    private static void hideKeyboard(Context context, View view){
        InputMethodManager imm = (InputMethodManager)context.getSystemService(
                Context.INPUT_METHOD_SERVICE);
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }
    private void printDensity(){
        float pxDip =  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, 1,
                getResources().getDisplayMetrics());
        float pxMM = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, 1,
                getResources().getDisplayMetrics());

        float pxSP =  TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_SP, 1,
                getResources().getDisplayMetrics());

        String density = String.format("density 1dip: %s, 1 mm: %s, sp: %s", pxDip, pxMM, pxSP);
        System.out.println(density);
        ViewUtils.setText(this, R.id.density, density);
    }

    @Override
    protected void onStart() {
        super.onStart();

        //saveBitmap();

    }

    int getPixels() {
        int dip = Integer.valueOf(widthField.getText().toString());
        float px = TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dip,
                getResources().getDisplayMetrics());
        Log.i("PIXELS", String.format("Width pixels %s", px));
        return (int) px;
    }

    public void saveBitmap(final boolean print) {

        Log.i("CLICK", "Calling save bitmap");
        final Runnable saveFile = new Runnable() {
            @Override
            public void run() {
                ticketGen.createTicket(getPixels(), new TicketGenerator.OnCreateBitmapListener() {
                    @Override
                    public void onCreate(final Bitmap bitmap) {

                        //Save file code
                        Runnable runn = new Runnable() {
                            @Override
                            public void run() {
                                final float width = 384.0f;
                                float percent = width/(float)bitmap.getWidth();
                                final float height = bitmap.getHeight()*percent;
                                File dir = new File(Environment.getExternalStorageDirectory(),"tickets_test");
                                String ticketFileName = "TICKET_TON.PNG";
                                TicketGenerator.saveFile(dir, ticketFileName, bitmap);
                                final Bitmap scaledBitmap = Bitmap.createScaledBitmap(bitmap, (int) width, (int) height, true);
                                ticketFileName = "scaled_ticket.png";
                                TicketGenerator.saveFile(dir, ticketFileName, scaledBitmap);

                                runOnUiThread(new Runnable() {
                                    @Override
                                    public void run() {
                                        widthField.clearFocus();
                                        ViewUtils.setImage(MyActivity.this, R.id.ticket, scaledBitmap);

                                        parent.requestFocus();
                                        Toast.makeText(MyActivity.this, "Imagen guardada", Toast.LENGTH_SHORT).show();
                                    }
                                });


                                if(print) {
                                    printImage(scaledBitmap);
                                }
                            }
                        };
                        Thread t = new Thread(runn);
                        t.start();
                    }
                });
            }
        };
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                Handler handler = new Handler(Looper.getMainLooper());
                handler.post(saveFile);


            }
        });
        t.start();
    }

    public Bitmap toGrayscale(Bitmap bmpOriginal)
    {
        int width, height;
        height = bmpOriginal.getHeight();
        width = bmpOriginal.getWidth();

        Bitmap bmpGrayscale = Bitmap.createBitmap(width, height, Bitmap.Config.RGB_565);
        Canvas c = new Canvas(bmpGrayscale);
        Paint paint = new Paint();
        ColorMatrix cm = new ColorMatrix();
        cm.setSaturation(0);
        ColorMatrixColorFilter f = new ColorMatrixColorFilter(cm);
        paint.setColorFilter(f);
        c.drawBitmap(bmpOriginal, 0, 0, paint);
        return bmpGrayscale;
    }


    public void printImage(Bitmap bitmap){
        BluetoothDevice device = devices.get(spinner.getSelectedItemPosition());
        //PrinterService.getInstance().printImage_CPCL(device, bitmap);
        PrinterService.getInstance().printOnS30(device, bitmap);
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                printButton.setEnabled(true);
            }
        });

    }
}
