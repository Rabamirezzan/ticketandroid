package com.example.PrintAll;

/**
 * Created by rramirezb on 08/04/2015.
 */
public interface OnSizeChangedListener {
    public void onSizeChanged(int w, int h, int ow, int oh);
}
