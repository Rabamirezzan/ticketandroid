package com.example.PrintAll;

import android.app.Activity;
import android.graphics.Bitmap;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

/**
 * Created by rramirezb on 22/04/2015.
 */
public class ViewUtils {

    public static void setText(Activity activity, int resourceId, CharSequence value){
        View v = activity.findViewById(resourceId);
        if(v instanceof TextView){
            ((TextView) v).setText(value);
        }
    }

    public static void setImage(Activity activity, int resourceId, Bitmap bitmap){
        View v = activity.findViewById(resourceId);
        if(v instanceof  ImageView) {
            ImageView imageView = (ImageView) v;
            imageView.setImageBitmap(bitmap);
        }
    }
}
