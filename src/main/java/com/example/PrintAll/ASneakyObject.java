package com.example.PrintAll;

import android.webkit.JavascriptInterface;

import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by rramirezb on 09/04/2015.
 */
public interface ASneakyObject {
    @JavascriptInterface
    public String getMessage();
}
