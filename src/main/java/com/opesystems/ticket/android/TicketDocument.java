package com.opesystems.ticket.android;

import android.webkit.JavascriptInterface;
import com.google.gson.Gson;

/**
 * Created by rramirezb on 09/04/2015.
 */
public abstract class TicketDocument {
    protected transient Gson gson;

    {
        gson = new Gson();
    }

    protected void setGson(Gson gson){
        this.gson = gson;
    }
    public abstract String getJavaScriptName();

    protected String parse(Object objectToJson){
        return gson.toJson(objectToJson);
    }

    @JavascriptInterface
    public String getContent(){
        return gson.toJson(this);
    }
}
