package com.opesystems.ticket.android;

import android.annotation.SuppressLint;
import android.annotation.TargetApi;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Build;
import android.os.Environment;
import android.util.TypedValue;
import android.view.View;
import android.view.ViewGroup;
import android.webkit.JavascriptInterface;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.Toast;
import com.example.PrintAll.PrinterService;
import com.example.PrintAll.R;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.util.UUID;

/**
 * Created by rramirezb on 09/04/2015.
 */
public class TicketGenerator {
    private  WebView webView;
    private final ViewGroup parent;
    private final String url;
    private final TicketDocument document;
    private static final long DELAY_FOR_REFRESH = 300;
    private final long DELAY;
    private final File ticketDir;
    private int webViewId;

    public TicketGenerator(ViewGroup parent, int webViewId, String url, TicketDocument document){
        this(parent,webViewId, url,document,DELAY_FOR_REFRESH);
    }

    public TicketGenerator(ViewGroup parent, int webViewId, String url, TicketDocument document, long delayForRefresh){
        this(parent, webViewId, url, document, delayForRefresh, parent.getContext().getCacheDir());

    }



    public TicketGenerator(ViewGroup parent, int webViewId, String url, TicketDocument document, File ticketDir){
        this(parent,webViewId, url, document, DELAY_FOR_REFRESH, ticketDir);
    }

//    public TicketGenerator(WebView webView, String url, TicketDocument document, long delayForRefresh, File ticketDir){
//        this.webView = webView;
//        this.url = url;
//        this.document = document;
//        DELAY = delayForRefresh;
//        this.ticketDir = ticketDir;
//        this.parent = null;
//        init();
//    }

    public TicketGenerator(ViewGroup parent, int webViewId, String url, TicketDocument document, long delayForRefresh, File ticketDir){
        this.parent = parent;
        this.url = url;
        this.document = document;
        DELAY = delayForRefresh;
        this.ticketDir = ticketDir;
        this.webViewId = webViewId;

    }

    private static WebView createWebViewLayouts(Context context, int webViewId, ViewGroup parent){
        LinearLayout layout = new LinearLayout(context);
        layout.setLayoutParams(new LinearLayout.LayoutParams(1,1));
        layout.setId(webViewId);

        ScrollView scroll = new ScrollView(context);
        scroll.setLayoutParams(new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.MATCH_PARENT));
        scroll.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        WebView webView = new WebView(context);
        //webView.setId(webViewId);

        scroll.addView(webView);
        layout.addView(scroll);

        View previous = parent.findViewById(webViewId);
        if(previous!=null) {
            parent.removeView(previous);
        }
        parent.addView(layout);

        return webView;
    }


    private void init(WebView webView){
        configureWebView(webView);
        configureTicketDocument(webView, document);
    }
    private static void configureWebView(WebView webView){
        webView.getSettings().setJavaScriptEnabled(true);
        webView.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);
        webView.setVerticalScrollBarEnabled(false);
        webView.setHorizontalScrollBarEnabled(false);
        webView.getSettings().setUseWideViewPort(true);

    }

    @SuppressLint("JavascriptInterface")
    private static void configureTicketDocument(WebView webView,TicketDocument document){
        if(webView!=null&&document!=null) {
            webView.addJavascriptInterface(document, document.getJavaScriptName());
        }
    }

    public String createTicket(int pixels, OnCreateBitmapListener listener){
        webView = createWebViewLayouts(parent.getContext(), webViewId, parent);
        init(webView);
        String ticketName = String.format("%s_ticket_%s.png", webView.getContext().getPackageName(), UUID.randomUUID());
        loadUrl(webView,url, ticketName, pixels, DELAY, listener);
        return ticketName;
    }


    public static int getWidthPixel(int mm, Context context){

        int px = (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_MM, mm,
                context.getResources().getDisplayMetrics());
        return px;
    }

    private static void loadUrl(
            final WebView webView,
            final String url,
            final String ticketFileName,
            final int pixels,
            final long delay,
            final OnCreateBitmapListener listener){
        resizeWebView( webView, pixels, 0);


        webView.loadUrl(url);
        webView.setWebViewClient(new WebViewClient() {
            int i = 0;
            @Override
            public void onPageFinished(WebView view, String _url) {
                System.out.println("RELOADED " + url + " " + view);
                if(i==0) {
                    i++;
                    updateWebViewSize(webView, ticketFileName, pixels, delay, listener);
                }
            }

        });
    }


    public static void updateWebViewSize(
            final WebView webView,
            final String ticketFileName,
            final int widthInPixel,
            final long delay,
            final OnCreateBitmapListener listener) {
        webView.post(new Runnable() {
            @TargetApi(Build.VERSION_CODES.HONEYCOMB)
            @Override
            public void run() {
                resizeWebView(webView, widthInPixel, 0);
                webView.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        Bitmap bitmap = createBitmap(webView);
                        listener.onCreate(bitmap);

                    }
                }, delay);
            }
        });
    }

    public static File getFile(File ticketDir, String fileName) {
        File dir = ticketDir;
        System.out.printf("Dir %s\n", dir.getAbsolutePath());
        dir.mkdirs();
        return new File(dir, fileName);
    }


    public static void saveFile(final File ticketDir, final String fileName, final Bitmap bitmap) {
        Thread t = new Thread(new Runnable() {
            @Override
            public void run() {

                try {
                    File f = getFile(ticketDir, fileName);

                    FileOutputStream file = new FileOutputStream(f);
                    bitmap.compress(Bitmap.CompressFormat.PNG, 100, file);
                } catch (FileNotFoundException e) {
                    e.printStackTrace();
                }
            }
        });
        t.start();
    }


    public static void resizeWebView( View child, int pixelWidth, int pixelHeight){


        View view = child;
        ViewGroup parent = ((ViewGroup) view.getParent());
        parent.removeView(view);



        int w = pixelWidth;
        int h = 0;//(int)(pixelHeight* child.getContext().getResources().getDisplayMetrics().density);//(int)px*4;
        view.invalidate();
        view.setLayoutParams(new LinearLayout.LayoutParams(w,
                LinearLayout.LayoutParams.WRAP_CONTENT));

        view.invalidate();
        view.refreshDrawableState();
        view.requestLayout();

        //Pre-measure the view so that height and width don't remain null.
        view.measure(w,//View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED),
                View.MeasureSpec.makeMeasureSpec(0, View.MeasureSpec.UNSPECIFIED));

        h = view.getMeasuredHeight();

        view.layout((int) view.getLeft(), (int) view.getTop(), w, h);
//        System.out.println(String.format("measured w:%s h:%s", w, h));
//        System.out.println(String.format("size w:%s h:%s", view.getWidth(), view.getHeight()));
        parent.addView(view);
        view.invalidate();
        view.refreshDrawableState();

    }
    public static Bitmap createBitmap( View child) {
        //  LayoutInflater mInflater = (LayoutInflater)context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View view = child;
        int w = view.getWidth();
        int h = view.getHeight();

//        System.out.printf("The new size w %s h %s\n", w,h);
        //Create the bitmap
        Bitmap bitmap = Bitmap.createBitmap(w,
                h,
                Bitmap.Config.ARGB_8888);

        //Create a canvas with the specified bitmap to draw into
        Canvas c = new Canvas(bitmap);

        //Render this view (and all of its children) to the given Canvas

        view.setDrawingCacheEnabled(true);
        view.invalidate();
        view.refreshDrawableState();
        view.buildDrawingCache();
        c.drawColor(Color.WHITE);
        view.draw(c);
        return bitmap;
    }


    public interface OnCreateBitmapListener {
        void onCreate(Bitmap bitmap);


    }
}
