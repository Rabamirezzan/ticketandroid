package com.opesystems.smartorder.ticket;

import android.webkit.JavascriptInterface;

/**
 * Created by rramirezb on 09/04/2015.
 */
public class Customer {
    private Integer id;
    private String name;
    private String age;
    public Customer(String name, String age) {
        this.name = name;
        this.age = age;
    }

    @JavascriptInterface
    public String getName() {
        return name;
    }

    @JavascriptInterface
    public String getAge() {
        return age;
    }
}
