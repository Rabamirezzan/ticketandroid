package com.opesystems.smartorder.ticket;

import android.webkit.JavascriptInterface;
import com.fasterxml.jackson.annotation.JsonSetter;
import com.google.gson.Gson;
import com.opesystems.ticket.android.TicketDocument;
import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by rramirezb on 09/04/2015.
 */
public class OrderTicket  extends TicketDocument{
    public String RFC;
    public String noteType;
    public String companyName;
    public String productsHeader;

    public EmployeeTicket employee;
    public CustomerTicket customer;
    public List<ProductTicket> selectedProducts;
    public List<ProductTicket> selectedPromotions;
    public List<ProductTicket> selectedReplacements;

    public String promotionsHeader;
    public String replacementsHeader;
    public String totalPaid;
    public String totalGifted;
    public String date;
    public String footer;

    public boolean showProducts = true;
    public boolean showPromotions = true;
    public boolean showReplacements = true;

    {
        selectedProducts = new ArrayList<ProductTicket>();
        selectedPromotions = new ArrayList<ProductTicket>();
        selectedReplacements = new ArrayList<ProductTicket>();
    }

    @Override
    public String getJavaScriptName() {
        return "ticket";
    }
}
