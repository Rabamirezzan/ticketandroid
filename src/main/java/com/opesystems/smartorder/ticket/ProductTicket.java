package com.opesystems.smartorder.ticket;

/**
 * Created by rramirezb on 10/04/2015.
 */
public class ProductTicket {
    public boolean highlight;
    public Integer id;
    public String code;
    public String name;
    public String amount;
    public String unitPrice;
    public String total;
    public boolean bold;

    public ProductTicket() {
    }

    public ProductTicket(Integer id, String code, String name, String amount, String unitPrice, String total, boolean highlight) {
        this.id = id;
        this.code = code;
        this.name = name;
        this.amount = amount;
        this.unitPrice = unitPrice;
        this.total = total;
        this.highlight = highlight;
    }


}
