package com.opesystems.utilities;

import com.opesystems.smartorder.ticket.ProductTicket;

import java.math.BigDecimal;
import java.math.RoundingMode;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

/**
 * Created by rramirezb on 23/04/2015.
 */
public class Products {

    private static final String[] names = new String[]{
            "Cristal Cbd 600 ml NRP 12 pack",
            "Cristal Negra 3LT NRP 6p",
            "Fanta Lata",
            "Coca Cola Zero Lata",
            "Powerade Moras 600 ml NRP 6Pzs",
            "Fresca 600ml NRP 12 Pack",
            "Fresca 3 Lts NRP 6pz",
            "Sprite 2L NRP 9P Cod Bar Nvo",
            "Mundet Manzana 2 L NRP 9 P",
            "Sprite 3Lts NRP 6P",
            "Coca Cola Life 600 ml NRP 12P",
            "Generosa Nectar de Mango 6Pack",
            "Generosa Jugo de Manzana 6Pzas",
            "Nutridefensas Nja 250ml NRP 6P",
            "Nutridefensas MznDrNja 250 6P",
            "Agua Purificada 1L",
            "Coca Cola 2L NRP 9C",
            "Coca Cola Light Lata",
            "Coca Cola Lata",
            "ACEITE SHELL RIMULA R4X 15W40 CI4 CUBETA 20 LT",
            "FILTRO DE ACEITE",
            "Cristal Nga 600 ml NRP 12 pack",
            "Coca Cola Light 600 ml NRP 12P",
            "APC NR 600 ml",
            "Coca Cola Zero 600ml NRP 12Pac",
            "Fanta 600ml NRP 12 Pack",
            "Fanta 3 Lts NRP 6pz",
            "Sprite 600ml NRP 12 P Etq. Nva",
            "Coca Cola 3 Lts NRP",
            "Mundet Manzana 600 ML NRP 12 P",
            "Fuze 500 ml NRP Te Verde 12 P",
            "Ciel Mini Mzna 300 ml NRP 6P",
            "Coca Cola Life 355 ml Lata 24P",
            "Generosa Nectar de Durazno 6Pz",
            "Valle Frut CPch 600 ML NRP 12P",
            "Nutridefensas Mzn-Ptno 250m 6P",
            "Fresca 2L NRP 9",
            "Fanta 2L NRP 9",
            "Coca Cola Light 2L NRP",
            "Coca Cola 2.5 Lts Nrp",
            "Cristal Nga 600 ml NRP 12 pack",
            "Coca Cola Light 600 ml NRP 12P",
            "Coca Cola Zero Lata",
            "Powerade Moras 600 ml NRP 6Pzs",
            "Fresca 600ml NRP 12 Pack",
            "Mundet Manzana 600 ML NRP 12 P",
            "Ciel Mini Mzna 300 ml NRP 6P",
            "Coca Cola Life 355 ml Lata 24P",
            "Valle Frut CPch 600 ML NRP 12P",
            "Cristal Te Lata",
            "Coca Cola Light Lata",
            "BANDA TIPO A LISA",
            "Cristal Cbd 600 ml NRP 12 pack",
            "APC NR 600 ml",
            "Coca Cola Zero 600ml NRP 12Pac",
            "Fanta 600ml NRP 12 Pack",
            "Sprite 600ml NRP 12 P Etq. Nva",
            "Fuze 500 ml NRP Te Verde 12 P",
            "Coca Cola Life 600 ml NRP 12P",
            "Generosa Nectar de Durazno 6Pz",
            "Nutridefensas Nja 250ml NRP 6P",
            "Agua Purificada 1L",
            "Coca Cola Lata",
            "Coca Cola 600ml NRP",
            "BANDA TIPO A LISA",
            "Cristal Nga 600 ml NRP 12 pack",
            "Cristal Cbd 600 ml NRP 12 pack",
            "Coca Cola Light 600 ml NRP 12P",
            "Coca Cola Zero Lata",
            "Coca Cola Zero 600ml NRP 12Pac",
            "Fanta 600ml NRP 12 Pack",
            "Fresca 600ml NRP 12 Pac",
            "Mundet Manzana 600 ML NRP 12 P",
            "Coca Cola Life 600 ml NRP 12",
            "Coca Cola Life 355ml Lata 12",
            "APC NR 600 ml P",
            "APC NR 1 Lt P",
            "Coca Cola Light Lat",
            "Coca Cola Lat",
            "Coca Cola 600ml NR",
            "BANDA TIPO A LIS",
            "Cristal Nga 600 ml NRP 12 pac",
            "Cristal Cbd 600 ml NRP 12 pac",
            "Coca Cola Light 600 ml NRP 12",
            "Coca Cola Zero Lat",
            "Coca Cola Zero 600ml NRP 12Pa",
            "Fanta 600ml NRP 12 Pac",
            "Fresca 600ml NRP 12 Pac",
            "Mundet Manzana 600 ML NRP 12 ",
            "Coca Cola Life 600 ml NRP 12",
            "Coca Cola Life 355ml Lata 12",
            "APC NR 600 ml P",
            "APC NR 1 Lt P",
            "Coca Cola Light Lat",
            "Coca Cola Lat",
            "Coca Cola 600ml NR"
    };

    public static class Values {
        public final double a;
        public final double p;
        public final double t;

        public Values() {
            a = Math.round(100 * Math.random() + 1);
            double pAux = 1000 * Math.random() + 1;
            BigDecimal bdP = BigDecimal.valueOf(pAux).setScale(2, RoundingMode.HALF_UP);

            p = bdP.doubleValue();
            t = a * p;
        }
    }

    public static List<ProductTicket> getProducts() {

        List<ProductTicket> products = new ArrayList<ProductTicket>();
        for (int i = 0; i < 20; i++) {
            int index = (int) (names.length * Math.random());
            String name = names[index];

            Values v = new Values();
            DecimalFormat df = new DecimalFormat("$#,###.00");
            DecimalFormat amountFormat = new DecimalFormat("0");

            String amount = String.format("%s", amountFormat.format(v.a));
            String unitPrice = df.format(v.p);
            String total = df.format(v.t);
            products.add(
                    new ProductTicket(
                            i, UUID.randomUUID().toString().substring(0, 10),
                            name, amount, unitPrice, total, false));

        }
        return products;
    }

    static String[] promotionsNames = new String[]{"Promoción 2x1", "Promoción Regalo Líquido", "Promoción Regalo Envase"};

    public static List<ProductTicket> getPromotions() {

        List<ProductTicket> products = new ArrayList<ProductTicket>();
        for (int i = 0; i < 25; i++) {
            //productNames.length * Math.random());

            boolean bold = (i % 5) == 0;



            Values v = new Values();
            DecimalFormat df = new DecimalFormat("$#,###.00");
            DecimalFormat amountFormat = new DecimalFormat("0");

            String amount = String.format("%s", amountFormat.format(v.a));
            String unitPrice = df.format(v.p);
            String total = df.format(v.t);
            if (bold) {
                int index = (int)(promotionsNames.length * Math.random());
                String name = promotionsNames[index];
                products.add(new ProductTicket(i, UUID.randomUUID().toString().substring(0, 5), name, amount, unitPrice, total, bold));

            } else {
                int index = i;
                String name = names[index];
                total = "";
                products.add(new ProductTicket(i, UUID.randomUUID().toString().substring(0, 5), name, amount, unitPrice, total, bold));
            }


        }
        return products;
    }
}
