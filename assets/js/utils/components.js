/**
 * Created by rramirezb on 08/04/2015.
 */


angular.module('app', [])

    .directive('tableRow', function(){
        return {
            restrict:'E',
            templateUrl:'../../components/table-row.html'
        }
    }

);