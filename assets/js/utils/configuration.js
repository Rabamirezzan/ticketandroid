/**
 * Created by rramirezb on 25/03/2015.
 */

var _ticket;
function init() {
    var date = new Date().toDateString();
    if (!window.ticket) {
        window.ticket = {
            customer: {
                name: 'La Tienda Todo Caro',
                address: 'Avenida Siempreviva 742, Springfield',
                code: 'RABR9876540AA'
            },
            employee: {name: 'Raúl Ramírez Bazán'},
            date: date,
            noteType: 'Nota de pedido',
            companyName: "OpeSystems - WorkByCloud",
            productsHeader: "Productos seleccionados",
            promotionsHeader: "Promociones seleccionadas",
            replacementsHeader: "Cambios",
            selectedProducts: getProducts(),
            selectedPromotions: getPromotions(),
            selectedReplacements: getReplacements(),
            showReplacements: true,
            footer: 'Los precios están sujetos a cambio sin previo aviso. Recuerde que usted solo sabe consumir, y' +
            ' nosotros sabemos como satisfacer sus necesidades de consumidor.'

        }

        window.ticket.showReplacements = window.ticket.selectedReplacements.length > 0;
        window.ticket.showProducts = window.ticket.selectedProducts.length > 0;
        window.ticket.showPromotions = window.ticket.selectedPromotions.length > 0;
        _ticket = JSON.stringify(window.ticket);
    } else {
        print("hay ticket");
        _ticket = window.ticket.getContent();
    }
}

init();


angular.module('app', []).controller('TicketController', ['$scope', function ($scope) {
    $scope.ticket = JSON.parse(_ticket);

}]).directive('productRow', function () {
        return {
            require: ['^table'],
            replace: true,
            restrict: 'A',
            template: '<tbody>' +
            '<tr ng-model="product">' +
            '<td colspan="5" class="wrap"  >{{product.name}}</td>' +
            '</tr><tr>' +

            '<td class="text-to-left">{{product.code}}</td>' +
            '<td class="text-to-right">{{product.amount}}</td>' +
            '<td class="text-to-right">{{product.unitPrice}}</td>' +
            '<td colspan="2" class="text-to-right">{{product.total}}</td>' +
            '</tr></tbody>'
            //'components/table-row.html'
        }
    }
).directive('promotionRow', function () {
        return {
            require: ['^table'],
            replace: true,
            restrict: 'A',
            template: '<tbody>' +
            '<tr ng-model="product"  bold="{{product.highlight}}">' +
            '<td colspan="4" class="wrap" underline="{{product.highlight}}">' +
            '<span show="{{product.highlight}}">&#x25BC</span>' +
            '{{product.name}}</td>' +
            '</tr><tr>' +
            '<td class="text-to-left">{{product.code}}</td>' +
            '<td class="text-to-right">{{product.amount}}</td>' +
            '<td colspan="2"  class="text-to-right">{{product.total}}</td>' +
            '</tr></tbody>'
            //'components/table-row.html'
        }
    }
).directive('replacementRow', function () {
        return {
            require: ['^table'],
            replace: true,
            restrict: 'A',
            template: '<tbody ng-model="product">' +
            '<tr   bold="{{product.highlight}}">' +
            '<td colspan="3" class="wrap" underline="{{product.highlight}}">{{product.name}}</td>' +
            '</tr><tr>' +

            '<td class="text-to-left">{{product.code}}</td>' +
            '<td colspan="2" class="text-to-right">{{product.amount}}</td>' +
            '</tr>' +
            '</tbody>'
            //'components/table-row.html'
        }
    }
);

function getMessage() {
    var time = "Default message";
    try {
        time = window.ticket.getCompanyName();
    }
    catch (e) {
        print('Exc ' + e.toString());
    }
    return time;
}