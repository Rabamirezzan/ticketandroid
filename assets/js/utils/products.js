/**
 * Created by rramirezb on 23/04/2015.
 */

var productNames = [
    "ACEITE SHELL RIMULA R4X 15W40 CI4 CUBETA 20 LT",
    "Cristal Cbd 600 ml NRP 12 pack",
    "Cristal Negra 3LT NRP 6p",
    "Fanta Lata",
    "Coca Cola Zero Lata",
    "Powerade Moras 600 ml NRP 6Pzs",
    "Fresca 600ml NRP 12 Pack",
    "Fresca 3 Lts NRP 6pz",
    "Sprite 2L NRP 9P Cod Bar Nvo",
    "Mundet Manzana 2 L NRP 9 P",
    "Sprite 3Lts NRP 6P",
    "Coca Cola Life 600 ml NRP 12P",
    "Generosa Nectar de Mango 6Pack",
    "Generosa Jugo de Manzana 6Pzas",
    "Nutridefensas Nja 250ml NRP 6P",
    "Nutridefensas MznDrNja 250 6P",
    "Agua Purificada 1L",
    "Coca Cola 2L NRP 9C",
    "Coca Cola Light Lata",
    "Coca Cola Lata",

    "FILTRO DE ACEITE",
    "Cristal Nga 600 ml NRP 12 pack",
    "Coca Cola Light 600 ml NRP 12P",
    "APC NR 600 ml",
    "Coca Cola Zero 600ml NRP 12Pac",
    "Fanta 600ml NRP 12 Pack",
    "Fanta 3 Lts NRP 6pz",
    "Sprite 600ml NRP 12 P Etq. Nva",
    "Coca Cola 3 Lts NRP",
    "Mundet Manzana 600 ML NRP 12 P",
    "Fuze 500 ml NRP Te Verde 12 P",
    "Ciel Mini Mzna 300 ml NRP 6P",
    "Coca Cola Life 355 ml Lata 24P",
    "Generosa Nectar de Durazno 6Pz",
    "Valle Frut CPch 600 ML NRP 12P",
    "Nutridefensas Mzn-Ptno 250m 6P",
    "Fresca 2L NRP 9",
    "Fanta 2L NRP 9",
    "Coca Cola Light 2L NRP",
    "Coca Cola 2.5 Lts Nrp",
    "Cristal Nga 600 ml NRP 12 pack",
    "Coca Cola Light 600 ml NRP 12P",
    "Coca Cola Zero Lata",
    "Powerade Moras 600 ml NRP 6Pzs",
    "Fresca 600ml NRP 12 Pack",
    "Mundet Manzana 600 ML NRP 12 P",
    "Ciel Mini Mzna 300 ml NRP 6P",
    "Coca Cola Life 355 ml Lata 24P",
    "Valle Frut CPch 600 ML NRP 12P",
    "Cristal Te Lata",
    "Coca Cola Light Lata",
    "BANDA TIPO A LISA",
    "Cristal Cbd 600 ml NRP 12 pack",
    "APC NR 600 ml",
    "Coca Cola Zero 600ml NRP 12Pac",
    "Fanta 600ml NRP 12 Pack",
    "Sprite 600ml NRP 12 P Etq. Nva",
    "Fuze 500 ml NRP Te Verde 12 P",
    "Coca Cola Life 600 ml NRP 12P",
    "Generosa Nectar de Durazno 6Pz",
    "Nutridefensas Nja 250ml NRP 6P",
    "Agua Purificada 1L",
    "Coca Cola Lata",
    "Coca Cola 600ml NRP",
    "BANDA TIPO A LISA",
    "Cristal Nga 600 ml NRP 12 pack",
    "Cristal Cbd 600 ml NRP 12 pack",
    "Coca Cola Light 600 ml NRP 12P",
    "Coca Cola Zero Lata",
    "Coca Cola Zero 600ml NRP 12Pac",
    "Fanta 600ml NRP 12 Pack",
    "Fresca 600ml NRP 12 Pac",
    "Mundet Manzana 600 ML NRP 12 P",
    "Coca Cola Life 600 ml NRP 12",
    "Coca Cola Life 355ml Lata 12",
    "APC NR 600 ml P",
    "APC NR 1 Lt P",
    "Coca Cola Light Lat",
    "Coca Cola Lat",
    "Coca Cola 600ml NR",
    "BANDA TIPO A LIS",
    "Cristal Nga 600 ml NRP 12 pac",
    "Cristal Cbd 600 ml NRP 12 pac",
    "Coca Cola Light 600 ml NRP 12",
    "Coca Cola Zero Lat",
    "Coca Cola Zero 600ml NRP 12Pa",
    "Fanta 600ml NRP 12 Pac",
    "Fresca 600ml NRP 12 Pac",
    "Mundet Manzana 600 ML NRP 12 ",
    "Coca Cola Life 600 ml NRP 12",
    "Coca Cola Life 355ml Lata 12",
    "APC NR 600 ml P",
    "APC NR 1 Lt P",
    "Coca Cola Light Lat",
    "Coca Cola Lat",
    "Coca Cola 600ml NR"
];

var promotionsNames = ["Promoción 2x1", "Promoción Regalo Líquido", "Promoción Regalo Envase"];

function getProducts() {

    var products = [];
    for (var i = 0; i < 5; i++) {
        var index = parseInt(i);//productNames.length * Math.random());
        var name = productNames[index];


        var a = Math.round(100 * Math.random() + 1);
        var p = 1000 * Math.random() + 1;
        p = parseFloat(p).toFixed(2);
        var t = a * p;
        var amount = parseInt(a);
        var unitPrice = '$' + p;
        var total = '$' + parseFloat(t).toFixed(2);
        var bold = (Math.random() * 3) > 2.0 ? true : false;
        products.push({
            'id': i, 'code': "12345", 'name': name, 'amount': amount,
            'unitPrice': unitPrice, 'total': total, 'highlight': bold
        });

    }
    return [];//products;
}

function getPromotions() {

    var products = [];
    for (var i = 0; i < 5; i++) {
        //productNames.length * Math.random());

        var bold = (i % 5) == 0;
        var a = Math.round(100 * Math.random() + 1);
        var p = 1000 * Math.random() + 1;
        p = parseFloat(p).toFixed(2);
        var t = a * p;
        var amount = parseInt(a);
        var unitPrice = '$' + p;
        var total = '$' + parseFloat(t).toFixed(2);
        if (bold) {
            var index = parseInt(promotionsNames.length * Math.random());
            var name = promotionsNames[index];
            products.push({
                'id': i, 'code': "[12345789]", 'name': name, 'amount': amount,
                'unitPrice': '', 'total': total, 'highlight': bold
            });
        } else {
            var index = parseInt(i);
            var name = productNames[index];
            products.push({
                'id': i, 'code': "[12345789]", 'name': name, 'amount': amount,
                'unitPrice': '', 'total': '', 'highlight': bold
            });
        }


    }
    return products;
    //return [];
}

function getReplacements() {

    var products = [];
    for (var i = 0; i < 5; i++) {
        //productNames.length * Math.random());

        var bold = false;
        var a = Math.round(100 * Math.random() + 1);
        var p = 1000 * Math.random() + 1;
        p = parseFloat(p).toFixed(2);
        var t = a * p;
        var amount = parseInt(a);
        var unitPrice = '$' + p;
        var total = '$' + parseFloat(t).toFixed(2);

        var index = parseInt(i);
        var name = productNames[index];
        products.push(
            {
                'id': i, 'code': "[12345789]",
                'name': name, 'amount': amount,
                'unitPrice': '', 'total': '', 'highlight': bold
            });

    }
    //return products;
    return [];
}